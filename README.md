# ONE FOR ALL - Movie List

This project is a React Application (that uses React, React Router, Bootstrap, Movie API)\
The App allows the user to search for movies, mark them as seen and favorites, and view information about them.\
The App also stores and displays your Favourites list.

## Getting Started

- Start by downloading the project files to a folder in your machine
- Open the project folder in your Code Editor
- Check if you have the correct project directory in your terminal
- Now in the project directory, you can run the following scripts:

```bash
npm install
```

Download and install the packages required for your Node.js project, making it easier for you to manage dependencies and reuse code.

```bash
npm start
```

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.\
The page will reload when you make changes.

### Prerequisites

Before you start, make sure you have the following software installed on your computer:
- Node.js (v16.16.0)
- npm (9.2.0)
- Code Editor that supports JavaScript such as Visual Studio Code

### Usage

In the Homepage the User can choose in the Search Bar from two different search options:
1. Search by Movie Title (and filter by Year)
    - The User enters a movie title
    - The "Year Field" is optional
2. Search by the correct IMDb ID
    - The User must enter a correct IMDb ID

Hovering over a movie the User can click on it to see the movie Info Page or can mark it as seen and favorite.\
The movies marked as favorites automatically appear in the "Your Favorites" list, at the top of the Homepage.

In the Info Page the User have the option to return to the Homepage, and can also mark the movie as seen and favorite.

----------------------------------------------------------------------------------------------------------------------

This App is pretty straightforward, but if you have any doubt you can email me at 'vascoalves.dev@gmail.com'.

### Contributing

If you want to contribute to the project, making suggestions, reporting bugs or making feature requests, you can email me at 'vascoalves.dev@gmail.com'.

### Author

VASCO ALVES - Software Developer\
02/2023