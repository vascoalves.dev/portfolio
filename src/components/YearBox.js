import React from "react";

const YearBox = (props) => {

    return (
        <div className="yearBox">
            <input type="number" min="1888" max={new Date().getFullYear()} className="form-control" value={props.value}
                onChange={(event) => props.setYearValue(event.target.value)}
                placeholder="Year..."></input>
        </div>
    )
}

export default YearBox;