import React from "react";
import { Link } from "react-router-dom";

const InfoList = (props) => {
    const HomeComponent = props.homeComponent;
    const SeenComponent = props.seenComponent;
    const FavouriteComponent = props.favouriteComponent;

    return (
        <>
            {props.movieInfo.map((movie, index) =>
                <div className="info-container">
                    <img src={movie.Poster} alt='movie' className="posterLarge"></img>

                    <div className="info-content">

                        <div className="infoPageButtons">
                            <Link to="/">
                                <div className="HomeBut">
                                    <HomeComponent />
                                </div>
                            </Link>
                            <div onClick={() => props.handleSeenClick(movie)} className="SeenBut">
                                <SeenComponent seen={props.seen} movie={movie} />
                            </div>
                            <div onClick={() => props.handleFavouritesClick(movie)} className="FavBut">
                                <FavouriteComponent favourites={props.favourites} movie={movie} />
                            </div>
                        </div>

                        <div className="infoText">

                            <h1 className="title">{movie.Title}</h1>

                            <div className="topInfo">

                                <div>

                                    <span>{movie.Year}</span>
                                    <span className="type">{movie.Type}</span>
                                    <span>{movie.Rated}</span>

                                    <span>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-clock" viewBox="0 0 16 16">
                                            <path d="M8 3.5a.5.5 0 0 0-1 0V9a.5.5 0 0 0 .252.434l3.5 2a.5.5 0 0 0 .496-.868L8 8.71V3.5z" />
                                            <path d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm7-8A7 7 0 1 1 1 8a7 7 0 0 1 14 0z" />
                                        </svg>
                                        {movie.Runtime}
                                    </span>

                                    <span>ID: {movie.imdbID}</span>

                                    <p>
                                        <span>Genre: {movie.Genre}</span>
                                        <span>Language: {movie.Language}</span>
                                    </p>

                                    <p>
                                        <span>Box Office: {movie.BoxOffice}</span>
                                    </p>

                                </div>

                                <div className="IMDBrating">

                                    <span className="rating">{movie.imdbRating} / 10</span>
                                    <span>{movie.imdbVotes} votes</span>

                                </div>

                            </div>

                            <h5 className="plot">{movie.Plot}</h5>
                            <p><strong>Director(s): </strong>{movie.Director}</p>
                            <p><strong>Writer(s): </strong>{movie.Writer}</p>
                            <p><strong>Star(s): </strong>{movie.Actors}</p>

                        </div>

                    </div>

                </div>)}
        </>
    )
}

export default InfoList;