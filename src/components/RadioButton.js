import React from "react";

const RadioButton = ({ label, value, onChange }) => {
    return (
        <div className="radioButton">
            <label className="radio">
                <input id="radio" type="radio" checked={value} onChange={onChange} />
                {label}
            </label>
        </div>
    );
};

export default RadioButton;