import React from "react";
import { Link } from "react-router-dom";

const MovieList = (props) => {
    const SeenComponent = props.seenComponent;
    const FavouriteComponent = props.favouriteComponent;

    return (
        <>
            {props.movies.map((movie, index) =>

                <div className="image-container">
                    <img src={movie.Poster} alt='movie' className="poster"></img>

                    <div className="grad">

                        <div className="topIcons">
                            <div className="topIconSeen"><SeenComponent seen={props.seen} movie={movie} /></div>
                            <div className="topIconFav"><FavouriteComponent favourites={props.favourites} movie={movie} /></div>
                        </div>

                        <div className="movieLabel">

                            <div className="labelline1">
                                {movie.Title}
                            </div>
                            <div className="labelline2">
                                <div>{movie.Year}</div>
                                <div>ID: {movie.imdbID}</div>
                            </div>

                        </div>

                    </div>

                    <div className="overlay">

                        <div className="overlayButtons">

                            <Link onClick={() => props.handleInfoClick(movie.imdbID)} to="info" className="overlayInfo">
                            </Link>
                            <div onClick={() => props.handleSeenClick(movie)} className="overlaySeen">
                                <SeenComponent seen={props.seen} movie={movie} />
                            </div>
                            <div onClick={() => props.handleFavouritesClick(movie)} className="overlayFav">
                                <FavouriteComponent favourites={props.favourites} movie={movie} />
                            </div>

                        </div>

                    </div>

                </div>)}
        </>
    )
}

export default MovieList;