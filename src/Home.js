import React, { useEffect, useState } from 'react';

import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';

import MovieList from './components/MovieList';
import MovieListHeading from './components/MovieListHeading';
import SearchBox from './components/SearchBox';
import YearBox from './components/YearBox';
import RadioButton from './components/RadioButton';
import SeenButton from './components/SeenButton';
import FavouritesButton from './components/FavouritesButton';

import { HandleFavouriteMovie, HandleSeenMovie } from './utils/Utils';

function Home({ GlobalState }) {

    const { setMovieInfo, seen, setSeen, favourites, setFavourites, searchValue, setSearchValue, showFavTitle, setShowFavTitle } = GlobalState;

    const [movies, setMovies] = useState([]);
    const [yearValue, setYearValue] = useState('');
    const [option, setOption] = useState('one');
    const [show, setShow] = useState(true);
    
    // localStorage.clear();   // Dev tools
    // console.log(localStorage);

    //---------------------------------------------------------------------------------------------
    // Requests Data from the API using the "s" Search Parameter and filters by year using the "y" Year Parameter
    //---------------------------------------------------------------------------------------------

    const getMovieRequest = async () => {
        const url = `http://www.omdbapi.com/?s=${searchValue}&y=${yearValue}&apikey=f79427b6`

        const response = await fetch(url);
        const responseJson = await response.json();

        setMovies(responseJson.Search);

        if (responseJson.Response === "False") {
            setMovies([]);   // Clear results if Search Field is empty
        }

    };

    //---------------------------------------------------------------------------------------------
    // Requests Data from the API using the "i" ID Parameter
    //---------------------------------------------------------------------------------------------

    const getMovieIDRequest = async () => {
        const url = `http://www.omdbapi.com/?i=${searchValue}&apikey=f79427b6`

        const response = await fetch(url);
        const responseJson = await response.json();

        var data = [];
        data.push(responseJson);

        if (responseJson.Response === "True") {   // Checks if the ID is valid
            setMovies(data);
        }
        else {
            setMovies([]);   // Clear Search if ID is invalid
        }

    };

    //---------------------------------------------------------------------------------------------
    // Requests Info from the API using the "i" ID Parameter
    //---------------------------------------------------------------------------------------------

    const getMovieInfoRequest = async (movieID) => {
        const url = `http://www.omdbapi.com/?i=${movieID}&apikey=f79427b6`

        const response = await fetch(url);
        const responseJson = await response.json();

        var data = [];
        data.push(responseJson);

        setMovieInfo(data);

        localStorage.setItem('react-movie-app-info', JSON.stringify(data));   // Save last Info request in the Local Storage

    };

    const HandleInfoClick = (movieID) => {
        getMovieInfoRequest(movieID);

    }

    //---------------------------------------------------------------------------------------------
    // Handle Search Field Mode, One = Search by Movie Title, Two = Search by IMDb ID
    //---------------------------------------------------------------------------------------------

    const handleOneChange = () => {
        setOption('one');
        setShow(true);
        setYearValue("");
        getMovieRequest(searchValue);   // Clear previous search

    };

    const handleTwoChange = () => {
        setOption('two');
        setShow(false);
        setYearValue("");
        getMovieIDRequest(searchValue);   // Clear previous search

    };

    //---------------------------------------------------------------------------------------------
    //  Call the API request after every Search Field change
    //---------------------------------------------------------------------------------------------

    useEffect(() => {

        if (option === "one") {
            // console.log(option);
            getMovieRequest(searchValue);
        }

        if (option === "two") {
            // console.log(option);
            getMovieIDRequest(searchValue);
        }

    }, [searchValue, yearValue]);

    //---------------------------------------------------------------------------------------------
    //  Get Favourites and Seen List from Local Storage on load
    //---------------------------------------------------------------------------------------------

    useEffect(() => {
        var movieFavourites = JSON.parse(localStorage.getItem('react-movie-app-favourites'));
        if (movieFavourites === null) {
            movieFavourites = [];
        }
        setFavourites(movieFavourites);

        var movieSeen = JSON.parse(localStorage.getItem('react-movie-app-seen'));
        if (movieSeen === null) {
            movieSeen = [];
        }
        setSeen(movieSeen);

        if (movieFavourites[0]) {      // Toggles Favourite List Title
            setShowFavTitle(true);
        }

    }, []);

    //---------------------------------------------------------------------------------------------
    // Calls utility functions
    //---------------------------------------------------------------------------------------------

    const HandleS = (movie) => {
        HandleSeenMovie(movie, seen, setSeen);
    }

    const HandleF = (movie) => {
        HandleFavouriteMovie(movie, favourites, setFavourites, setShowFavTitle);
    }

    //---------------------------------------------------------------------------------------------
    //---------------------------------------------------------------------------------------------

    return (
        <div className='container-fluid movie-app'>

            <div className='content'>
                <div className='logo logoHome'></div>

                {showFavTitle &&
                    <div className='favTitle'>
                        <MovieListHeading heading='YOUR FAVOURITES' />
                    </div>
                }

                <div className='movieList'>
                    <MovieList movies={favourites} favourites={favourites} handleFavouritesClick={HandleF} favouriteComponent={FavouritesButton} seen={seen} handleSeenClick={HandleS} seenComponent={SeenButton} handleInfoClick={HandleInfoClick} />
                </div>

                <div className='searchHead'>

                    <div className='boxes'>
                        <SearchBox searchValue={searchValue} setSearchValue={setSearchValue} />
                        {show && <YearBox yearValue={yearValue} setYearValue={setYearValue} />}
                    </div>

                    <div className='radio'>

                        <RadioButton
                            label=" Search by Movie Title (and filter by Year)"
                            value={option === 'one'}
                            onChange={handleOneChange}
                        />
                        <RadioButton
                            label=" Search by the correct IMDb ID"
                            value={option === 'two'}
                            onChange={handleTwoChange}
                        />

                    </div>

                </div>

                <div className='movieList'>
                    <MovieList movies={movies} favourites={favourites} handleFavouritesClick={HandleF} favouriteComponent={FavouritesButton} seen={seen} handleSeenClick={HandleS} seenComponent={SeenButton} handleInfoClick={HandleInfoClick} />
                </div>

            </div>
        </div>
    );
}

export default Home;