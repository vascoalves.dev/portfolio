//------------------------------------- UTILITY FUNCTIONS -------------------------------------
//---------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------
// Save Favourites and Seen in Local Storage
//---------------------------------------------------------------------------------------------

const saveFavToLocalStorage = (items) => {
    localStorage.setItem('react-movie-app-favourites', JSON.stringify(items));
};

const saveSeenToLocalStorage = (items) => {
    localStorage.setItem('react-movie-app-seen', JSON.stringify(items));
};

//---------------------------------------------------------------------------------------------
// Add and Remove Movie in Favourites List
//---------------------------------------------------------------------------------------------

export const HandleFavouriteMovie = (movie, favourites, setFavourites, setShowFavTitle) => {
    const check = favourites.filter((favourite) => favourite.imdbID === movie.imdbID);

    if (check.length === 0) {   //Checks if movie is allready on favourites
        const newFavouriteList = [...favourites, movie];
        setFavourites(newFavouriteList);

        saveFavToLocalStorage(newFavouriteList);

        if (newFavouriteList[0]) {   // Toggles Favourite List Title
            setShowFavTitle(true);
        } else {
            setShowFavTitle(false);
        }
    }

    else {
        const newFavouriteList = favourites.filter((favourite) => favourite.imdbID !== movie.imdbID);
        setFavourites(newFavouriteList);

        saveFavToLocalStorage(newFavouriteList);

        if (newFavouriteList[0]) {   // Toggles Favourite List Title
            setShowFavTitle(true);
        } else {
            setShowFavTitle(false);
        }
    }
}

//---------------------------------------------------------------------------------------------
// Add and Remove Movie in Seen List
//---------------------------------------------------------------------------------------------

export const HandleSeenMovie = (movie, seen, setSeen) => {
    const check = seen.filter((seen) => seen.imdbID === movie.imdbID);

    if (check.length === 0) {   //Checks if movie is allready on seen
        const newSeenList = [...seen, movie];
        setSeen(newSeenList);

        saveSeenToLocalStorage(newSeenList);
    }

    else {
        const newSeenList = seen.filter((seen) => seen.imdbID !== movie.imdbID);
        setSeen(newSeenList);

        saveSeenToLocalStorage(newSeenList);
    }
}
