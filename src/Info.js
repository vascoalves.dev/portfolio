import React, { useEffect } from 'react';

import InfoList from './components/InfoList';
import HomeButton from './components/HomeButton';
import SeenButton from './components/SeenButton';
import FavouritesButton from './components/FavouritesButton';

import { HandleFavouriteMovie, HandleSeenMovie } from './utils/Utils';

function Info({ GlobalState }) {

    const { movieInfo, setMovieInfo, seen, setSeen, favourites, setFavourites, setShowFavTitle } = GlobalState;

    //---------------------------------------------------------------------------------------------
    //  Get Info from Local Storage on load (Hold Info on refresh)
    //---------------------------------------------------------------------------------------------

    useEffect(() => {
        var movieInfo = JSON.parse(localStorage.getItem('react-movie-app-info'));

        if (movieInfo === null) {
            movieInfo = [];
        }

        setMovieInfo(movieInfo);

    }, []);

    //---------------------------------------------------------------------------------------------
    // Calls utility functions
    //---------------------------------------------------------------------------------------------

    const HandleS = (movie) => {
        HandleSeenMovie(movie, seen, setSeen);
    }

    const HandleF = (movie) => {
        HandleFavouriteMovie(movie, favourites, setFavourites, setShowFavTitle);
    }

    //---------------------------------------------------------------------------------------------
    //---------------------------------------------------------------------------------------------

    return (

        <div className='container-fluid movie-app'>

            <div className='content'>

                <div className='logo logoInfo'></div>

                <div className='movieInfo'>
                    <InfoList movieInfo={movieInfo} favourites={favourites} handleFavouritesClick={HandleF} favouriteComponent={FavouritesButton} seen={seen} handleSeenClick={HandleS} seenComponent={SeenButton} homeComponent={HomeButton} />
                </div>

            </div>
        </div>

    )
}

export default Info;