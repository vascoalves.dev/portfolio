import React, { useState } from "react";
import { Routes, Route } from "react-router-dom"

import Home from "./Home"
import Info from "./Info"

function App() {

  // hook variables
  const [movieInfo, setMovieInfo] = useState([]);
  const [seen, setSeen] = useState([]);
  const [favourites, setFavourites] = useState([]);
  const [searchValue, setSearchValue] = useState('');
  const [showFavTitle, setShowFavTitle] = useState(false);

  // GlobalState object
  const GlobalState = {
    movieInfo, setMovieInfo,
    seen, setSeen,
    favourites, setFavourites,
    searchValue, setSearchValue,
    showFavTitle, setShowFavTitle
  }

  return (
    <div className="App">
      <Routes>
        <Route path="/" element={<Home GlobalState={GlobalState} />} />
        <Route path="info" element={<Info GlobalState={GlobalState} />} />
      </Routes>
    </div>
  );
}

export default App;